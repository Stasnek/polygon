package generator;

import generator.functions.ExpressionPart;
import org.nfunk.jep.type.Complex;

/**
 * Created by User on 27.03.2016.
 */
public class ComplexTeX extends Complex implements ExpressionPart {
    private final String IM_IDENT = "i";
    private String identificator;

    public ComplexTeX(double real, double im, int index) {
        super(real, im);
        setIdentificator(new String("Z_" + index));
    }


    public String toStringDetailedWithIdent() {
        if (im() > 0)
            return new String(identificator + ": " + re() + "+" + im() + IM_IDENT);
        else return new String(identificator + ": " + String.valueOf(re()) + String.valueOf(im()) + IM_IDENT);

    }

    public String toStringJEP() {
        if (im() > 0)
            return new String("(" + re() + "+" + im() + "*" + IM_IDENT + ")");
        else return new String("(" + re() + im() + "*" + IM_IDENT + ")");

    }

    public String toString() {
        return identificator;
    }

    public void setIdentificator(String identificator) {
        this.identificator = identificator;
    }
}