package generator;

import generator.functions.ExpressionPart;
import generator.functions.Functions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 25.04.2016.
 */
public class ExpressionNode {
    private StringBuilder texExpression = new StringBuilder();
    private int level = 0;
    private int deep = 0;
    private StringBuilder jepExpression = new StringBuilder();
    private List<Functions> functions = new ArrayList<Functions>();
    private List<String> traces = new ArrayList<String>();
    private List<ComplexTeX> constants = new ArrayList<ComplexTeX>();


    private List<ExpressionPart> expressionSeq = new ArrayList<ExpressionPart>();

    public ExpressionNode(ExpressionNode node) {
        level = node.getLevel();
        deep = node.getDeep();
        functions = node.getFunctions();
        constants = node.getConstants();
        expressionSeq = node.getExpressionSeq();
    }

    public ExpressionNode() {
    }

    public List<ExpressionPart> getExpressionSeq() {
        return expressionSeq;
    }

    public void setExpressionSeq(List<ExpressionPart> expressionSeq) {
        this.expressionSeq = expressionSeq;
    }

    public List<String> getTraces() {
        return traces;
    }

    public int getDeep() {
        return deep;
    }

    public void setDeep(int deep) {
        this.deep = deep;
    }

    public void deepInc() {
        deep++;
    }

    public void levelInc() {
        level++;
    }

    public void levelDec() {
        level--;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Functions> getFunctions() {
        return functions;
    }

    public void setFunctions(List<Functions> functions) {
        this.functions = functions;
    }

    public StringBuilder getTexExpression() {
        return texExpression;
    }

    public StringBuilder getJepExpression() {
        return jepExpression;
    }

    public void addFunction(Functions func) {
        functions.add(func);
        this.expressionSeq.add(func);
    }

    public void addConstant(ComplexTeX constant) {
        constants.add(constant);
    }

    public void addTrace(String trace) {
        traces.add(trace);
    }

    public ExpressionNode appendToTeX(String str) {
        texExpression.append(str);
        return this;
    }

    public ExpressionNode appendToJEP(String str) {
        jepExpression.append(str);
        return this;
    }

    public ExpressionNode insertToTeX(int index, String str) {
        texExpression.insert(index, str);
        return this;
    }

    public ExpressionNode insertToJEP(int index, String str) {
        jepExpression.insert(index, str);
        return this;
    }

    public String toString() {
        return String.valueOf(texExpression);
    }

    public void printInfo() {
        System.out.println("\nlevel : " + level);
        System.out.println("deep : " + deep);
        System.out.println("TeXexpr : " + texExpression);
        System.out.println("JEPexpr : " + jepExpression);
        System.out.println("funcs : " + functions);
        System.out.print("constants : [");
        for (ComplexTeX constant : constants) {
            System.out.print(constant.toString() + ", ");
        }
        System.out.print("]\n");
        System.out.print("sequence : [");
        for (ExpressionPart ePart : expressionSeq) {
            System.out.print(ePart + ", ");
        }
        System.out.print("]\n");
//        System.out.println("trace : ");
//        for (String trace : traces) {
//            System.out.println(trace);
//        }
//        System.out.println();
    }

    public void appendConstant(ComplexTeX randomIdentificator) {
        this.appendToTeX(randomIdentificator.toString());
        this.addConstant(randomIdentificator);
        this.expressionSeq.add(randomIdentificator);
    }

    public void appendConstantJEP(ComplexTeX randomIdentificator) {
        this.appendToJEP(randomIdentificator.toString());
    }

    public List<ComplexTeX> getConstants() {
        return constants;
    }

    public void setConstants(List<ComplexTeX> constants) {
        this.constants = constants;
    }
}
