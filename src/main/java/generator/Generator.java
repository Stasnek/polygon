package generator;

import generator.functions.ExpressionPart;
import generator.functions.FunctionMakerJEP;
import generator.functions.FunctionMakerTeX;
import generator.functions.Functions;
import generator.util.MathUtil;
import org.nfunk.jep.JEP;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by User on 27.03.2016.
 */
public class Generator {

    private final static int MAX_POW_VALUE = 30;
    private final static int ROUND_VALUE = 2;
    private final static int MAX_COMPLEX_VALUE = 10;
    private final static int MAX_COMPLEX_NUMBERS_IN_EXPRESSION = 10;
    private final static int MAX_DEEP = 3;
    private Random random = new Random();
    private int constantsInExpessionCount = 0;
    private int vars = 4;
    private int varCount = 0;
    private JEP parser = new JEP();
    private List<ComplexTeX> constants = new ArrayList<ComplexTeX>();
    private StringBuilder texExpression = new StringBuilder();
    private StringBuilder texTopic = new StringBuilder();
    private StringBuilder jepExpression = new StringBuilder();
    private List<Functions> funcs = new ArrayList<Functions>();


    public Generator() {
        parser.addComplex();
        parser.addStandardFunctions();
        parser.addStandardConstants();
        funcs.add(Functions.DIV);
        funcs.add(Functions.PLUS);
        funcs.add(Functions.MULT);
        funcs.add(Functions.MINUS);
        funcs.add(Functions.SIN);
        // funcs.add(Functions.COS);

    }

    private ComplexTeX generateComplexNumber() {
        Double realPart = random.nextInt(MAX_COMPLEX_VALUE) + random.nextDouble();
        realPart = MathUtil.round(realPart, ROUND_VALUE);
        Double imaginaryPart = random.nextInt(MAX_COMPLEX_VALUE) + random.nextDouble();
        imaginaryPart = MathUtil.round(imaginaryPart, ROUND_VALUE);

        if (random.nextBoolean() == false) {
            realPart = -realPart;
        }

        if (random.nextBoolean() == false) {
            imaginaryPart = -imaginaryPart;
        }

        return new ComplexTeX(realPart, imaginaryPart, ++varCount);
    }

    private void generateConstants() {
        ComplexTeX complexNumber;
        for (int i = 0; i < vars; i++) {
            complexNumber = generateComplexNumber();
            constants.add(complexNumber);
            parser.addVariableAsObject(complexNumber.toString(), complexNumber);
        }

    }

    private void generateTopic() {
        for (int i = 0; i < vars; i++) {
            texTopic.append(constants.get(i).toStringDetailedWithIdent()).append(";\\\\");
        }
    }

    private Boolean hasNext(ExpressionNode node) {
        int value = random.nextInt(MAX_DEEP);
        int level = node.getLevel();
        System.out.println("попал в barrier at " + constantsInExpessionCount + " порог=" + value + " level " + level);
        node.addTrace("попал в barrier at " + constantsInExpessionCount + " порог=" + value + " level " + level);
        if (node.getLevel() == 0 & value > 0) {
            return true;
        }
        if (value > level) {
            return true;
        }
        return false;
    }

    private Functions randomizeOperation(int level) {
        return funcs.get(random.nextInt(funcs.size()));
    }

    private ComplexTeX randomizeIdentificator(ExpressionNode node) {
        int constantIndex = random.nextInt(vars);
        System.out.println("level : " + node.getLevel() + " : " + constants.get(constantIndex).toString() + " constantsInExpessionCount: " + constantsInExpessionCount);
        node.addTrace("level : " + node.getLevel() + " : " + constants.get(constantIndex).toString() + " constantsInExpessionCount: " + constantsInExpessionCount);
        return constants.get(constantIndex);
    }

    private void updateNode(ExpressionNode node, ExpressionNode subNode) {
        // node.setFunctions(subNode.getFunctions());
        //node.setConstants(subNode.getConstants());
        node.setDeep(subNode.getDeep());
    }


    private void makeOperation(ExpressionNode node) {
        Functions func = randomizeOperation(node.getLevel());
        System.out.println(func);
        switch (func) {
            case PLUS:
                FunctionMakerTeX.plus(node);
                node.addFunction(func);
                break;
            case MINUS:
                FunctionMakerTeX.minus(node);
                node.addFunction(func);
                break;
            case DIV:
                if (node.getDeep() < MAX_DEEP) {
                    node.levelInc();
                    node.deepInc();
                    node.addFunction(func);
                    node.addFunction(Functions.IN);
                    //creating subNode | diving to recursion
                    ExpressionNode subNode = generateTexExpression(new ExpressionNode(node));
                    //make operation for tex & jep exprs
                    FunctionMakerTeX.div(node, subNode);
                    //getting information from subNodes
                    updateNode(node, subNode);
                    node.addFunction(Functions.OUT);
                    if (constantsInExpessionCount < MAX_COMPLEX_NUMBERS_IN_EXPRESSION) {
                        node.levelDec();
                        makeOperation(node);
                    }
                } else {
                    makeOperation(node);
                }
                break;
            case MULT:
                node.addFunction(func);
                FunctionMakerTeX.mult(node);
                break;
            case SIN:
                if (node.getDeep() < MAX_DEEP) {
                    node.levelInc();
                    node.deepInc();
                    node.addFunction(func);
                    // node.addFunction(Functions.IN);
                    //make operation for tex & jep exprs
                    FunctionMakerTeX.sin(node);
                    //  node.addFunction(Functions.OUT);
                    if (constantsInExpessionCount < MAX_COMPLEX_NUMBERS_IN_EXPRESSION) {
                        node.levelDec();
                        makeOperation(node);
                    }
                } else {
                    makeOperation(node);
                }
                break;
        }
    }

    private ExpressionNode generateTexExpression(ExpressionNode node) {
        if (constantsInExpessionCount < MAX_COMPLEX_NUMBERS_IN_EXPRESSION & node.getLevel() < MAX_DEEP) {
            // node.printInfo();
            constantsInExpessionCount++;
            node.appendConstant(randomizeIdentificator(node));
            if (!hasNext(node)) {
                if (node.getLevel() == 0 & constantsInExpessionCount != MAX_COMPLEX_NUMBERS_IN_EXPRESSION) {
                    makeOperation(node);
                }
                return node;
            } else {
                if (constantsInExpessionCount != MAX_COMPLEX_NUMBERS_IN_EXPRESSION) {
                    makeOperation(node);
                    generateTexExpression(node);
                }
            }
        }
        return node;
    }


    public void start() {
        generateConstants();
        generateTopic();
        List<ExpressionNode> expressionNodes = new ArrayList<ExpressionNode>();
        int nodesCount = 0;
        while (constantsInExpessionCount < MAX_COMPLEX_NUMBERS_IN_EXPRESSION) {
            expressionNodes.add(generateTexExpression(new ExpressionNode()));
            nodesCount++;
            System.out.println("\nnode : " + nodesCount);
        }
        System.out.println(expressionNodes);
        for (ExpressionNode node : expressionNodes) {
            generateJepExpression(node);
            jepExpression.append(node.getJepExpression());
            texExpression.append(node.getTexExpression());
            node.printInfo();
        }
        parser.parseExpression(new String(jepExpression));
        parser.getErrorInfo();
        System.out.println(jepExpression);
    }

    private void generateJepExpression(ExpressionNode node) {
        Functions func;
        ComplexTeX complexTeX;
        FunctionMakerJEP makerJEP = new FunctionMakerJEP(node);
        for (ExpressionPart ePart : node.getExpressionSeq()) {
            if (ComplexTeX.class.isInstance(ePart)) {
                complexTeX = (ComplexTeX) ePart;
                node.appendToJEP(complexTeX.toString());
            } else {
                func = (Functions) ePart;
                switch (func) {
                    case PLUS:
                        makerJEP.plus(node);
                        break;
                    case MINUS:
                        makerJEP.minus(node);
                        break;
                    case DIV:
                        makerJEP.div(node);
                        break;
                    case MULT:
                        makerJEP.mult(node);
                        break;
                    case IN:
                        makerJEP.in(node);
                        break;
                    case OUT:
                        makerJEP.out(node);
                        break;
                    case SIN:
                        makerJEP.sin(node);
                        break;
                }
            }
        }
    }


    public String getTexExpression() {
        return "$" + texTopic + "\\\\" + texExpression + "\\\\" + parser.getComplexValue() + "$";
    }
}
