package generator.functions;

import generator.ExpressionNode;

/**
 * Created by User on 28.04.2016.
 */
public interface FunctionMaker {
    void plus(ExpressionNode node);

    void mult(ExpressionNode node);

    void minus(ExpressionNode node);

    void div(ExpressionNode node);

}
