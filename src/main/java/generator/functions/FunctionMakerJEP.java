package generator.functions;

import generator.ExpressionNode;

/**
 * Created by User on 28.04.2016.
 */
public class FunctionMakerJEP {
    private ExpressionNode node;
    private int lastInIndex = 0;
    private int level = 0;

    public FunctionMakerJEP(ExpressionNode node) {
        this.node = node;
    }

    public void plus(ExpressionNode node) {
        node.appendToJEP("+");
    }

    public void minus(ExpressionNode node) {
        node.appendToJEP("-");
    }

    public void mult(ExpressionNode node) {
        node.appendToJEP("*");
    }

    public void div(ExpressionNode node) {
        if (level > 0)
            node.insertToJEP(lastInIndex, "(");
        else node.insertToJEP(0, "(");

        node.appendToJEP(")");
        node.appendToJEP("/");
    }

    public void in(ExpressionNode node) {
        level++;
        lastInIndex = node.getJepExpression().length();
        node.appendToJEP("(");
    }

    public void out(ExpressionNode node) {
        level--;
        node.appendToJEP(")");
    }

    public void sin(ExpressionNode node) {

        //if (level > 0)
        node.insertToJEP(0, "sin(");
        //    else
        //      node.insertToJEP(lastInIndex, "sin(");

        node.appendToJEP(")");
    }
}
