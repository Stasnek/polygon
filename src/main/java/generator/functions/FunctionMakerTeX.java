package generator.functions;

import generator.ExpressionNode;

/**
 * Created by User on 26.04.2016.
 */
public class FunctionMakerTeX {
    public static void plus(ExpressionNode node) {
        node.appendToTeX("+");
    }

    public static void minus(ExpressionNode node) {
        node.appendToTeX("-");
    }

    public static void mult(ExpressionNode node) {
        node.appendToTeX("*");
    }

    public static void div(ExpressionNode node, ExpressionNode subNode) {
        node.insertToTeX(0, "\\frac{").appendToTeX("}{");
        node.appendToTeX(String.valueOf(subNode.getTexExpression()));
        node.appendToTeX("}");
    }

    public static void sin(ExpressionNode node) {
        node.insertToTeX(0, "\\sin(");
        node.appendToTeX(")");

    }
}
