package generator.functions;

/**
 * Created by User on 21.04.2016.
 */
public enum Functions implements ExpressionPart {
    PLUS, MINUS, DIV, MULT, IN, OUT, SIN, COS
}
