package generator.util;

import java.math.BigDecimal;

/**
 * Created by User on 28.03.2016.
 */
public class MathUtil {
    public static double round(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }
}
