package test;

import generator.ComplexTeX;
import org.nfunk.jep.Node;

/**
 * Created by User on 19.04.2016.
 */
public class JEPtest {


    public static void main(String[] args) {
        org.nfunk.jep.JEP parser = new org.nfunk.jep.JEP();
        parser.addComplex();
        parser.addStandardFunctions();
        parser.addStandardConstants();
        parser.addComplexVariable("Z", 1, 5);
        // parser.addVariableAsObject("Z",new ComplexTeX(1,5,1));
        try {
            parser.parseExpression("cos(sin(Z)^0.23+1)*pi");
            System.out.println(parser.getComplexValue());
            System.out.println("Re: " + parser.getComplexValue().re());
            System.out.println("Im: " + parser.getComplexValue().im());
            System.out.println(parser.getSymbolTable());
            System.out.println(parser.getErrorInfo());
            Node node = parser.getTopNode();
            try {
                do {
                    System.out.println(node.toString());

                    node = node.jjtGetChild(0);


                } while (node.jjtGetChild(0) != null);
            } catch (NullPointerException e) {
                // e.printStackTrace();
                System.out.println("catched!");//GraphCanvas graphCanvas=new
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
