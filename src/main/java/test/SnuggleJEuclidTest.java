package test;

import net.sourceforge.jeuclid.MathMLParserSupport;
import net.sourceforge.jeuclid.MutableLayoutContext;
import net.sourceforge.jeuclid.context.LayoutContextImpl;
import net.sourceforge.jeuclid.context.Parameter;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import uk.ac.ed.ph.snuggletex.SnuggleEngine;
import uk.ac.ed.ph.snuggletex.SnuggleInput;
import uk.ac.ed.ph.snuggletex.SnuggleSession;
import net.sourceforge.jeuclid.converter.Converter;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Node;

/**
 * Created by User on 26.03.2016.
 */
public class SnuggleJEuclidTest {

    public static BufferedImage createImage(String texExpression) {
        BufferedImage texImage = null;
        /* Create vanilla SnuggleEngine and new SnuggleSession */
        SnuggleEngine engine = new SnuggleEngine();
        SnuggleSession session = engine.createSession();

/* Parse some very basic Math Mode input */
        SnuggleInput input = new SnuggleInput(texExpression);
        try {
            session.parseInput(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

/* Convert the results to an XML String, which in this case will
 * be a single MathML <math>...</math> element. */
        String xmlString = session.buildXMLString();
        Converter converter = Converter.getInstance();
        Document document = null;
        try {
            document = MathMLParserSupport.parseString(xmlString);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        final MutableLayoutContext params = new LayoutContextImpl(
                LayoutContextImpl.getDefaultLayoutContext());
        params.setParameter(Parameter.MATHSIZE, 260f);
        try {
            texImage = converter.render(document, params);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return texImage;
    }
}
