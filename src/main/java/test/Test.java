package test;

/**
 * Created by �� on 3/25/2016.
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import generator.Generator;
import org.apache.commons.lang.time.StopWatch;

/**
 * A class to test LaTeX rendering.
 **/
public class Test {
    public final static String TEX_IMAGE_NAME_SNUGGLE = "SnuggleTexEuclid.png";
    public final static String TEX_IMAGE_NAME_JLATEX = "jLaTeXMath.png";

    public final static String EXPRESSION_0 = "$$ a^2 = b^2 + c^2 - b^2 + c^2 -  b^2 + c^2 - \\exp $$";

    public final static String EXPRESSION_1 = "\\frac{1}{\\alpha\\sqrt{2\\pi}}\n" +
            "\\exp\\left(-\\frac{(x-\\mu)^2}{2\\alpha^2}\\right)";

    public final static String EXPRESSION_2 = "$E = mc^2\\\\\n" +
            "    m_1 = \\frac{m_0}{\\sqrt{1-\\frac{v^2}{c^2}}}$";

    public final static String EXPRESSION_3 = "\\LaTeX{}";

    public final static String EXPRESSION_4 = "$\\pi\\approx 3,14$";

    public final static String EXPRESSION_5 = "\\begin{array}{l} \\forall\\varepsilon\\in\\mathbb{R}_+^*\\ \\exists\\eta>0\\ " +
            "|x-x_0|\\leq\\eta\\Longrightarrow|f(x)-f(x_0)|\\leq\\varepsilon\\\\\\det\\begin{bmatrix}a_{11}&a_{12}&\\cdots&a_{1n}\\\\a_{21}&\\ddots&&\\" +
            "vdots\\\\\\vdots&&\\ddots&\\vdots\\\\a_{n1}&\\cdots&\\cdots&a_{nn}\\end{bmatrix}\\overset{\\mathrm{def}}{=}\\sum_{\\sigma\\in\\mathfrak{S}_n}" +
            "\\varepsilon(\\sigma)\\prod_{k=1}^n a_{k\\sigma(k)}\\\\" +
            "\\sideset{_\\alpha^\\beta}{_\\gamma^\\delta}{\\begin{pmatrix}a&b\\\\c&d\\end{pmatrix}}\\\\" +
            "\\int_0^\\infty{x^{2n} e^{-a x^2}\\,dx} = \\frac{2n-1}{2a} \\int_0^\\infty{x^{2(n-1)} e^{-a x^2}\\,dx} = \\frac{(2n-1)!!}{2^{n+1}} \\sqrt{\\frac{\\pi}{a^{2n+1}}}\\\\" +
            "\\int_a^b{f(x)\\,dx} = (b - a) \\sum\\limits_{n = 1}^\\infty  {\\sum\\limits_{m = 1}^{2^n  - 1} {\\left( { - 1} \\right)^{m + 1} } } 2^{ - n} f(a + m" +
            "\\left( {b - a} \\right)2^{-n} )\\\\" +
            "\\int_{-\\pi}^{\\pi} \\sin(\\alpha x) \\sin^n(\\beta x) dx = \\textstyle{\\left \\{ \\begin{array}{cc} (-1)^{(n+1)/2} (-1)^m \\frac{2 \\pi}{2^n} \\binom{n}{m} & n \\mbox{ odd},\\ " +
            "\\alpha = \\beta (2m-n) \\\\ 0 & \\mbox{otherwise} \\\\ \\end{array} \\right .}\\\\" +
            "L = \\int_a^b \\sqrt{ \\left|\\sum_{i,j=1}^ng_{ij}(\\gamma(t))\\left(\\frac{d}{dt}x^i\\circ\\gamma(t)\\right)\\left(\\frac{d}{dt}x^j\\circ\\gamma(t)\\right)\\right|}\\,dt\\\\" +
            "\\begin{array}{rl} s &= \\int_a^b\\left\\|\\frac{d}{dt}\\vec{r}\\,(u(t),v(t))\\right\\|\\,dt \\\\ &= \\int_a^b \\sqrt{u'(t)^2\\,\\vec{r}_u\\cdot\\vec{r}_u + 2u'(t)v'(t)\\, \\vec{r}_u" +
            "\\cdot\\vec{r}_v+ v'(t)^2\\,\\vec{r}_v\\cdot\\vec{r}_v}\\,\\,\\, dt. \\end{array}\\\\" +
            "\\end{array}";


    public final static String EXPRESSION_6 = "$$\\sqrt[3,7]{x}$$";

    public final static String EXPRESSION_7 = "$Z_1$";

    public static void saveImage(BufferedImage image, String imageName) {
        File file = new File(imageName);
        try {
            ImageIO.write(image, "png", file.getAbsoluteFile());
        } catch (IOException ex) {
        }
    }

    private static long getJLateXTotalRenderTime(int laps, String expr) {
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < laps; i++) {
            JLaTeXMathTest.createImage(expr);
        }
        return timer.getTime();
    }

    private static long getSnuggleTotalRenderTime(int laps, String expr) {
        StopWatch timer = new StopWatch();
        timer.start();
        for (int i = 0; i < laps; i++) {
            SnuggleJEuclidTest.createImage(expr);
        }
        return timer.getTime();
    }


    public static void main(String[] args) {
        int lapsCount = 10;


//        long snuggleTime = getSnuggleTotalRenderTime(lapsCount, EXPRESSION_0);
//        long latexTime = getJLateXTotalRenderTime(lapsCount, EXPRESSION_6);
//
//        System.out.println("Snuggle total render time is " + snuggleTime);
//        System.out.println("jLateX total render time is " + latexTime);
//
//        System.out.println("Snuggle avg render time is " + (double) snuggleTime / lapsCount);
//        System.out.println("jLateX avg render time is " + (double) latexTime / lapsCount);
        Generator generator = new Generator();
        generator.start();
        BufferedImage jLatexImage = JLaTeXMathTest.createImage(generator.getTexExpression());
        // BufferedImage jLatexImage=JLaTeXMathTest.createImage(EXPRESSION_7);
        TestForm.displayImage(jLatexImage);
        //BufferedImage jLatexImage = JLaTeXMathTest.createImage(EXPRESSION_5);

        //saveImage(jLatexImage, TEX_IMAGE_NAME_JLATEX);
//        saveImage(snuggleImage, TEX_IMAGE_NAME_SNUGGLE);

    }
}