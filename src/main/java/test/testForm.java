package test;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by User on 27.03.2016.
 */
public class TestForm {
    private JPanel panel1;
    private JTextArea textArea1;

    public void gf() {
        textArea1 = new JTextArea();
    }

    public static void displayImage(BufferedImage image) {

        ImageIcon icon = new ImageIcon(image);
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(image.getWidth() + 10, image.getHeight() + 100);
        JLabel lbl = new JLabel();
        JButton jButton = new JButton();
        jButton.setText("Generate!");
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.add(jButton);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
